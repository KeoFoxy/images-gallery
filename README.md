# :art: Image Gallery

## :computer: Технологии

- **Next.js:** 
- **React**
- **Redux Toolkit:** 
- **Tailwind CSS:** 
- **MUI Icons:**

## :mag_right: Превью
Вы также можете оценить проект [**здесь**](https://images-gallery-keo.vercel.app/)

## :wrench: Запуск

```bash
npm run dev
```

## :hammer: Функционал

- Добавление изображений по URL(Есть проверка URL с помощью regex).
- Добавление комментариев к изображениям сразу или потом.
- Редактирование комментариев.
- Удаление изображений.
- Превью изображения.
- Адаптив

## :clipboard: Задание
Спроектировать и реализовать галерею изображений на TS.
Требования:
  - ~~Добавление изображения (url и комментарий)~~
  - ~~Удаление изображения~~
  - ~~Вывод загруженных изображений с комментариями~~
  - ~~Вид: маленькое превью изображения, снизу комментарий~~
  - ~~Выводятся построчно на весь экран, скролл только вертикальный~~
  - ~~Просмотр изображения по клику на превью (изображение максимум на 80% экрана, закрывается по клику вне изображения)~~
  - ~~Комментарий к изображению должен редактироваться~~
  - ~~Сохранение состояния при обновлении страницы (хранилище по выбору)~~
  - ~~Использование React + любой state manager~~
