export interface CommentaryProps {
    text: string;
    onUpdate: (newComment: string) => void;
}